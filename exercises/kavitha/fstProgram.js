console.log("hello");

console.log('hello world');
console.log('hello %s', 'world');
const name = 'Will Robinson';
console.warn(`Danger ${name}! Danger!`);

const os = require('os');


console.log(process.env);
console.log(process.env['TERM']);

process.emitWarning('Something happened!', 'CustomWarning', 'WARN001');
 


console.log(os.arch());
console.log(os.cpus());
console.log(os.homedir())
console.log(os.hostname());


var fs = require('fs');

//Blocking code
var data = fs.readFileSync('hello.js');
console.log(data.toString());
console.log("File ended");

//Non blocking code // callback

fs.readFile('hello.js', function(err, data)  {
if(err){
  console.log(err);
} else {
  console.log(data.toString());
}
});

console.log("program ended");
