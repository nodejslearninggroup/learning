console.log("hello");

var http = require('http');
var url = require('url');

http.createServer(function(req, res) {
      var parsedUrl = url.parse(req.url);
      console.log(parsedUrl);
      res.writeHead(200, {'content-type' : 'text/html'});
      res.write("Hello");
      res.end();
}).listen(8080);

console.log("Server started successfully");