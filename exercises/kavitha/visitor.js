var http = require('http');
var url = require('url');
var dateformat = require('dateformat');
var events = require('events');
var fs = require('fs');
var em = new events.EventEmitter();
console.log('Server started successfully');
http.createServer(function(req, res){
     const parsedUrl = url.parse(req.url, true);
     var count = 1 ;
     var visitor = 1;
     console.log(parsedUrl);
     if(parsedUrl.pathname === '/') {
         if(fs.existsSync('visitor.txt')) {
             fs.readFile('visitor.txt', function(err, data) {
                 if(data.length > 0){
                     visitor = parseInt(data) + count;
                 } else {
                     console.log('No data present');
                 }
                 em.emit('visitor', visitor, parsedUrl.query.user);
                 res.write('Hi, ' + parsedUrl.query.user + ' you are the visitor number ' + visitor);
                 res.end();
             });
         } else {
            em.emit('visitor', visitor, parsedUrl.query.user);
            res.write('Hi, ' + parsedUrl.query.user + ' you are the visitor number ' + visitor);
            res.end();
         }
         res.writeHead(200, {'content-type' : 'text/html'});
     } else {
        res.writeHead(404, {'content-type' : 'text/html'});
        res.write('Error Not Found');
        res.end();
     }
}).listen(8080);

em.on('visitor', function(visitor, username) {
    fs.writeFile('visitor.txt', visitor+'', (err) => {
        if(err){
            console.log(err);
        } else {
            console.log('File saved successfully');
        }
    });
    var date = new Date();
    var day = dateformat(date, 'isoDate');
    var time = dateformat(date, 'isoTime');
    var writeContent = username + ' visited the page on ' + day + ' ' + time;
    if(fs.existsSync('visitorlog.txt')){
        console.log('success')
        fs.readFile('visitorlog.txt', function(err, data){
            fs.writeFile('visitorlog.txt', data + '\n' + writeContent, (err) => {
                console.log('File saved successfully');
            });
        });
    } else {
        console.log('fail')
        fs.writeFile('visitorlog.txt', writeContent, (err) => {
            console.log('File saved successfully');
        });
    }
});