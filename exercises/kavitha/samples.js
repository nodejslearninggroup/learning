var URL = require('url').URL;

const myURL = new URL("https://user:pass@example.com:8080/path?query=que#hash");

console.log(myURL)
console.log(myURL.hash)
console.log(myURL.host)
console.log(myURL.hostname)
console.log(myURL.href)
console.log(myURL.origin)
console.log(myURL.password)
console.log(myURL.pathname)
console.log(myURL.port)
console.log(myURL.protocol)
console.log(myURL.search)
console.log(myURL.searchParams)
console.log(myURL.username)
console.log(myURL.toJSON())

console.log(`${process.arch}`)
console.log(process.argv)